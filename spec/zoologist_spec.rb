require "zoologist"
require "zoologist/animal"
require "zoologist/animal_info"

RSpec.describe Zoologist do
  before :all do
    @gnu = Zoologist::Animal.new("gnu")
    @penguin = Zoologist::Animal.new("penguin")
    @snake = Zoologist::Animal.new("snake")
    @snek = Zoologist::Animal.new("snek")
    @centipede = Zoologist::Animal.new("centipede")
    @displacer = Zoologist::Animal.new("displacement beast")
    @crumple_horned_snorkack = Zoologist::Animal.new("crumple-horned snorkack")
  end

  it "has a version number" do
    expect(Zoologist::VERSION).not_to be nil
  end

  it "gives correct information" do
    gnu_info = Zoologist::AnimalInfo.provide_info @gnu
    penguin_info = Zoologist::AnimalInfo.provide_info @penguin
    snake_info = Zoologist::AnimalInfo.provide_info @snake
    snek_info = Zoologist::AnimalInfo.provide_info @snek
    centipede_info = Zoologist::AnimalInfo.provide_info @centipede
    displacer_info = Zoologist::AnimalInfo.provide_info @displacer
    crumple_horned_snorkack_info = Zoologist::AnimalInfo.provide_info @crumple_horned_snorkack
    
    expect(gnu_info).to eq("The best animal in existence.")
    expect(penguin_info).to eq("A bird-like creature that is fine in both freezing and hot temperatures.")
    expect(snake_info).to eq("Did you mean 'snek'?")
    expect(snek_info).to eq("sssssssssSSSSSSSSsssssssslithery.")
    expect(centipede_info).to eq("There is nothing at all wrong with centipedes.")
    expect(displacer_info).to eq("Displacement beasts are losers.")
    expect(crumple_horned_snorkack_info).to eq("This animal does not exist.")
  end

  it "gives correct vocalizations" do
    expect(@gnu.vocalize).to eql("interjection!")
    expect(@penguin.vocalize).to eql("noot noot!")
    expect(@snake.vocalize).to eql("*snake noises*")
    expect(@snek.vocalize).to eql("sssssssssSSSSSSSSssssssss!")
    expect(@centipede.vocalize).to eql("AAAUUUUGGGGHHH!")
    expect(@displacer.vocalize).to eql("arf!")
    expect(@crumple_horned_snorkack.vocalize).to eql("*crumple-horned snorkack noises*")
  end
end
